<?php
$icons_preffix = 'fas fa-';
return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | Here you can change the default title of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#61-title
    |
    */

    'title' => 'Enzo',
    'title_prefix' => '',
    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | Here you can change the logo of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#62-logo
    |
    */

    'logo' => '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE0AAAAbCAYAAAA53gJaAAABG2lDQ1BpY2MAACjPY2BgMnB0cXJlEmBgyM0rKQpyd1KIiIxSYD/PwMbAzAAGicnFBY4BAT4gdl5+XioDBvh2jYERRF/WBZnFQBrgSi4oKgHSf4DYKCW1OJmBgdEAyM4uLykAijPOAbJFkrLB7A0gdlFIkDOQfQTI5kuHsK+A2EkQ9hMQuwjoCSD7C0h9OpjNxAE2B8KWAbFLUitA9jI45xdUFmWmZ5QoGFpaWio4puQnpSoEVxaXpOYWK3jmJecXFeQXJZakpgDVQtwHBoIQhaAQ0wBqtNAk0d8EASgeIKzPgeDwZRQ7gxBDgOTSojIok5HJmDAfYcYcCQYG/6UMDCx/EGImvQwMC3QYGPinIsTUDBkYBPQZGPbNAQDAxk/9PAA7dgAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH4wsOARYRvOF6vAAAAV96VFh0UmF3IHByb2ZpbGUgdHlwZSBpY2MAADjLnVTZjYQwDP1PFVuCb5NymECk7b+BdSBhYIU0h1FAenbs54v0W0r6aaIGCZoQg6CIkCgQbpAttro4KYkLEeikWWcC8FKbOg7DSZKhsbOHIwUFKUNhq9U4Cm9IjaiNEQ5gYVoOZh9K+tB+Dv7g5NK59AyYkomps+354kjbHu5RIegXMPcLKEahO/AHDDxFOSUK2h2VHglW8zO+PPGL/XrgzdHWj11R5YjsJ5xgejI641iejFpq08gZQNhqUM9Ols0tNASMnNtD0dsoRY30NAaCw4rbfVucUyhztLldJ4f2NtyU3fWlhocgXlzGrKU2bDT1mBPlT9v+bfu/d3SsxkkqaxviMciIFkt3Z3gnMYgaVbR/MMaebvVLrwxe6QeRS2q54DYvUud916SWO8Ys09K+M9A+RzXrbWpkjD1s+2XAY80l/QF3Q+fh33T4dgAADnNJREFUaN7tmWmUXVWVx3/73Hvfe/VqrlRV5lQGE8IQEgwRYwIEZVABATHQtmEQUVFWWhQQhFaQpajYhiGIC+ykBYdlo2AgNIIKCVPEJCQQAhFIJSSVSipDpapeveG+O5zdH16lUgUvsbK6P7rXulXv3nPPHv57n733uUcoQ4sTCYAUynBVKyKCqiIiHJZEiL1kTzLX2xV++KMg4uq6lxop+i6ATaSKPfOv3lvh93LDIw/0T7s7XYV1vCrHzzViY6yb8PMNI3e7UVGNWvGy3cNNHKU4EjImO/XGb+/rfevvfPq3v2L961t4+tLLEcfBWbsCM35qg+7rODaR8EZJKpWKc9meMFXVGn349LfclY8H8Td/An99hhue+OX7WLvl5NkgAJgOLAUqFBT6/hweNYco+mkIP3bCIiLSHKk+qDAOQGz8bt3KR6/UKGxbPOd0Fr70l9K0Qh4ROVPV3oGqqxL/TeP481LI5Z1CLo2NFlvVWUA8RMhcMWZZ56oXrzmg9rOnzcWtqSOYdlIi0do0X9q3fJE4mmZzWgliQCM3l93r/Ol3K2x13T3VN1++rufoWfywcQw37tvxj0HrowpgInAEHlbEarMCxsYg4qD6AWA8gMTRFO3uvMDmMvcMu+IrcAA0tYhSA0zqY9QOGLEWE4cGGAu0HEmgqerIIJPBSSS4c+YpkM+iXqKm4plHvyOF7FckjtMD9QY8icJxEoWXSRTM8avrb5y1ac0ja4+Zxd1Tj+VrLz49JNAUsKUAkgwib6Jq/4GujoppdeyggDg4RxUC/1JGT3h41y8e6Lj9q9/lpvtuKcdnoJyBehQQ2YhqWEbXJlSn9DNIVLQX/rZKK879DHHYTfTBU9zEsqXXmXz2GtQ6gOJ6b5BIrpE46lGRSQTBbGzcKGHwAWP1rldHtuxz3nzluRyDzT4caAdJ5FW3tv7TxjG+9X10MCgIgqlIoVYJxA2T+/cceh1F0XTNZj6Z6Olc2lQesMPRFqeq+kIjdAKlBGuVeMRYidu23i5+fgqAut42W9/4UCHbw7bly2nwhMRbGz5m/PxC1DqIhOp695thzXeEu9p29Ihow9mfTejLK042mc6fEIbTJQ7HOF17vxMcc+J8Nwr2Lz7rfBYuvhUAMyRVVSOCYhZrc4jk5D1X6ZnJiUjOqA30MPVC1LqOn1+QP352XcdRJ/DrXz09JBX64ilWv5C1YZQ/oEegkgsyvdOIwgv7HKxaUfnAzI7t67NzzqZ+2gnEJ56akkLuixJHdQDqJZcHoybcbKOozb32Bzq73WK69gZRZ8cztnbYNzDOHgAT+HO9jm1nuW+/2g/Y0COtpIxoFHF1b6b8eC476PZnh2FlwuLsZFvrGdq153etC86idsg69OkRR9iPXUTxrY3EtY2V3voXrpUoHFUCJPFyNGzk0tW1jZiuPTg7tuB4iSkSFueWhJseTVct9nZuzURhwHWLboJFN/HQl7/BviAkc+bFK2sXXfeo+PmrsDYh+d7z/E9d/vsf79oWXr9mRYnF0N38fyTjFDSV3gKAtSnJZy43x59UWT152mEm6QGoSnqqOv1Dv1+KbFyDt3H1hcbPnw2gxsnZdPWd8u6mju07NlM1bQZO1x4kl5ktNm4GwPFe0zET1+mU6WRmntHP7tL7FxF37KD2jq9ZkhVPIMYHkCiYlVzz7BinbXP/u0OONBEUlJ9WVZXuHRdQNIqxyRTFsVNw8r18/Z3XyzOwcZdNpG43YfB9iaPhhOEpdnvrPM3n/udQnrOOh3UTMeibQArkTXG9CIXiyPHIuCktZte2r2NtCkCTqWXRUTOWO937mHH+xVhx6AaSUTAVLSUN9bzXUhtXZ+xX/53b7vveIHnX72hlUdMoROQNjOmQ2I6X2I6QbKZFg2Brv/+HiJirrlepcVyBarp02bTavt9Wk2otag9bXI2Exb/ZROppALFxFX7+0t4Pzkuqlyg7IUqm8auHFfza5n/za5vn+TWNX/Zrm3uLx32I3VvfFbN/95clCmYAqOvtsDUNd7lvrPUJilz5o2+R+a/FMOGYhFo7ud8UG7f6wIgvLSyv5MSjkHGTOsWYvuZMK1Cd5BTzRxhpqjOi7u4nQMsVRceG0Sp/f9e3XD/b3wqoHChvAxQq5PyovvkhLRbOFxvXmDA4s7J1w2xVVh6mdijQnzCTY8cTbVjDiHGj50ghfwWqIKI2Xf3AN3a3rf3B6fORlkmwZBOpTCdk9jlxGFaWFDA2rKzb71qLv3ZVWWHO+pdAxLdR2N1nu1DINwC8vWUnUyaOGjJoNaCzDzOeQ61BD+4ZRHk/auDo9JNeZNWfnyWIz5c4qnNzmQUzo2Dl+kNsOL7Vtav/9/fnnU+xbTM68ejqROvG6yQOhwNYL7k6bhq15D9qGjC5Xm5c8kMAvNo61FoTF4seVgGJvcp0lmwXC668oKwpPWOn0jtpug5//hHf8UvRZWxkBio31JxWAHYC5dafA7Shg40uF2kA7srlReqbfkEUnIm1aVMsnL1uxNgZ0tH2jxpn8jfczYhPtBDWDptviv7HSxaZvFbVLPJaN+58EPjtipVc0OevZ046HrWqB5tyNXEUewZ4ZsVrfOy06e+TkdzdhuSzSBQOwEZUBpg31OZ2nbjuJRqGPmWwUJEiDO7SDxFpUFVDOGHqs+6br7xo/PyZxNEIzXRdImJeR+2hZvGjY2bhXjaH4rjJE9xd27+G2iSATVY8Fk06brkZ3sO5777L45PHYPqYuKNbiLq7rREpimqp+nZ31mvR56Pzji9ravWUo9A4coI929J9thOn0oFTONhSDbXlKHpV1e3iOLsoRdx7r04Grk0OvbmXbAZv3Qu9cUXVQ2pMAGCK/kVSP+zjh5p2y6kX4AxrIrPkWWO69l4lUXA8gDpuu62qu8t5Y20h3LuHZH296yVSCzxjbnFTFVd7o8fV7M52F0mk2ktKKY4wUoCff/fesvrliiH5wFar4zb3PQod192qCFMmjgKOoOVQVePV1fGVzs73D4ZF2NU6VFb4E47FOu5TTr53rRQLH8HGYzTXexGHiLLG5/5A5CWpvWzuySaf/Xxf8kdT6aWr9+xYfeI5C+CvK9A49tTaK9Ta08TG24njJ8ak05kY6W+yNIqOjS++ysu9/Px796/ced4l8OKfUNcbJXFUQkhMxrqJ7dQNg+59wJE0t6qUkunQ6HBf3tZvfYOKza91aiL1S8TEqELRLzvl3o+eS9ByFNGkY2sk13utxFETgHqJtVHjiAc+NHoCsmt7v5YczLsWFK2uQyur12BMKavH4Ux57skW1v+Ve06a1y9n2WPP8dJjv4TO3ZhsZg7WNgKo47yt9cO2am1D/7tHsI0qXW9vbu+/taoEQYiNFWME1zV4nou18MxFFx6S1dwvXEv41KOI6z6mfu5LEgYnHOrdxBevJ/7sKcR1jfNNUDwLQI3xbWXNnd62t3e0Ate8+BIPnzirvNr1jWg295qa/e+ItdMlisdLz/6LawvZ72fHTeTeWXMpNI9jxyXnMbd5NKQrm6V96+dQNSDYRGpF9ZZN3dmz5sO2t48INE23TAy7X1vHkx8YPXiAg4noQKg4gHvChw8ZbQuX/IT//M0KMv962i6q638tUTjjQMf+XspcfREyasIkZ+/Oa1CbANBk+g+FeRf8Pr1sCZM0ZvnE0aSAjg+dE1V2tg+qwk5VFf5br3dIVe2vJJeZjqqYYmFhb7rqjfya55elq2tJbtpAHBRxj5tZZV99+SaNotkA6rrtUXX977oTKcyG1UcYaSLjMm9u/Kpx3YCBXb8IGCMiRkBRa7HivGLC4mpEUBE5FHKFZb8hHjMJdb1HE37+CxIWj36f2ESSnq/fbRpu/fxVEgbHlR4aK0Ku+vEll2Jw0P4Mo6PWPZVQq2MPuFARUEXGTiZOpn5N2+ZzTLFwKjYeTiH/s4qKyhO1qvYFR+NsEKbH6iurPiOB/ylUDWJiW1V3f/sdf9jQeN9NfPvlp44QNGsnqw3uKTsWx4NKnhD9yIqsdg4GYvloe/jn3Pql73DMA7dtbauu/283Cm59TwHG7e2i4ebPpU0YfKSflVoj+eyVCleW0XOwj/oONera3iGfTO2K6puv1/17lkjgT0PtCCnkb6bo+zEaOZDCWrdvnrXJ1IPR6An3jL7hQlusqh8k5nCFQCittCMjY0ygShSGREHgKuL1KeJIImnEPegnb8smto8cj60d9rC63oAP8eqYKMAR8BKegA499x7gAG4cBE4cBGw//V8o3v4gYcf2NXHz6AVaUfkkYmJQsHEKa6sOAKaO2x1X1d0ejmi5Nm7f1hPt38e331k32JnlBDq1dYC0YuMbUXVxPce6btKEgU8cxwePpRTECMYYrFrUikmm1mzbt4cxXgobx1mvoekeU8i5gma8puF7rV+AHaVq95n77+LJy68Ex9lsCj3XmqDYAuCItLtEQWLECNzausDf/PfFGkWjGLgjESnJVRQbv3c3IeolckGsPQC3/eW3LJ4wDvcvm8l/4rgNOvnYy9j57ifJZmZLFKZKhiCaSu/Vqpo/5uac+0Jy1R8juvdw8ksbYM7gz1dlM84Pyww6DO0oyACvAJP7LKzv+69Aoo/HdQPeX3zMCWAMhY2v9K9lBSIgDSQbhpHb3zmEk7DBZIE8BgFu68P6T0+8wJpzTiY98WhkyyaqgS2qNBmHa2zMfSL4XpJg6kxsGDDmm7dw6RXnvo93WdCe+vNqQHGM4DoenR176drVTtPE8dTV1BJZgBhjIIqUfC5HIllBMplErcUzLjt378HGMT0722ka30LDiCZsX593xrwZ/bKeff51wnwRryKJqoJwMJCtoqpoHHKgoUVAraIKhXwOMYaKdAWiig44lxUxeMnSQjp17uA95qIFC3GSSewjv8D0NawA8fijGL7175ilj/PZL5x3hG76J/2T/r/pfwFivftvxM8yYwAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxOS0xMS0xNFQwMToyMjoxNyswMDowML0Cr7wAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTktMTEtMTRUMDE6MjI6MTcrMDA6MDDMXxcAAAAAN3RFWHRpY2M6Y29weXJpZ2h0AENvcHlyaWdodCAxOTk5IEFkb2JlIFN5c3RlbXMgSW5jb3Jwb3JhdGVkMWz/bQAAACB0RVh0aWNjOmRlc2NyaXB0aW9uAEFkb2JlIFJHQiAoMTk5OCmwuur2AAAAAElFTkSuQmCC" class="img-fluid">',
    'logo_img' => 'imgs/icon-enzo2.png',
    'logo_img_class' => 'brand-image-xl',
    'logo_img_xl' => null,
    'logo_img_xl_class' => 'brand-image-xs',
    'logo_img_alt' => 'AdminLTE',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Here we change the layout of your admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#63-layout
    |
    */

    'layout_topnav' => null,
    'layout_boxed' => null,
    'layout_fixed_sidebar' => true,
    'layout_fixed_navbar' => true,
    'layout_fixed_footer' => true,

    /*
    |--------------------------------------------------------------------------
    | Extra Classes
    |--------------------------------------------------------------------------
    |
    | Here you can change the look and behavior of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#64-classes
    |
    */

    'classes_body' => '',
    'classes_brand' => '',
    'classes_brand_text' => '',
    'classes_content_header' => 'container-fluid',
    'classes_content' => 'container-fluid',
    'classes_sidebar' => 'sidebar-dark-red elevation-2 ',
    'classes_sidebar_nav' => 'nav-hide nav-child-indent nav-legacy nav-red nav-collapse-hide-child',
    'classes_topnav' => 'navbar navbar-dark bg-red',
    'classes_topnav_nav' => 'navbar-expand-md',
    'classes_topnav_container' => 'container',

    /*
    |--------------------------------------------------------------------------
    | Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#65-sidebar
    |
    */

    'sidebar_mini' => true,
    'sidebar_collapse' => true,
    'sidebar_collapse_auto_size' => true,
    'sidebar_collapse_remember' => false,
    'sidebar_collapse_remember_no_transition' => true,
    'sidebar_scrollbar_theme' => 'os-theme-light',
    'sidebar_scrollbar_auto_hide' => 'l',
    'sidebar_nav_accordion' => true,
    'sidebar_nav_animation_speed' => 300,

    /*
    |--------------------------------------------------------------------------
    | Control Sidebar (Right Sidebar)
    |--------------------------------------------------------------------------
    |
    | Here we can modify the right sidebar aka control sidebar of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#66-control-sidebar-right-sidebar
    |
    */

    'right_sidebar' => false,
    'right_sidebar_icon' => 'fas fa-cogs',
    'right_sidebar_theme' => 'dark',
    'right_sidebar_slide' => true,
    'right_sidebar_push' => true,
    'right_sidebar_scrollbar_theme' => 'os-theme-light',
    'right_sidebar_scrollbar_auto_hide' => 'l',

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Here we can modify the url settings of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#67-urls
    |
    */

    'use_route_url' => false,

    'dashboard_url' => 'home',

    'logout_url' => 'logout',

    'login_url' => 'login',

    'register_url' => 'register',

    'password_reset_url' => 'password/reset',

    'password_email_url' => 'password/email',

    /*
    |--------------------------------------------------------------------------
    | Laravel Mix
    |--------------------------------------------------------------------------
    |
    | Here we can enable the Laravel Mix option for the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#68-laravel-mix
    |
    */

    'enabled_laravel_mix' => false,

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Here we can modify the sidebar/top navigation of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#69-menu
    |
    */

    'menu' => [
        [
            'text' => 'Início',
            'route'  => 'home',
            'icon' => $icons_preffix . 'home',
            'icon-color' => 'red',
        ],
        [
            'text'    => 'Cadastros',
            'icon'    => 'fas fa-fw fa-share',
            'submenu' => [
                [
                    'text' => 'Grupos',
                    'route'  => 'grupo.index',
                    'icon' => $icons_preffix . 'layer-group',
                ],
                [
                    'text' => 'Atividades',
                    'route'  => 'atividade.index',
                    'icon' => $icons_preffix . 'graduation-cap',
                ],
                [
                    'text' => 'Certificados',
                    'route'  => 'evento.index',
                    'icon' => $icons_preffix . 'graduation-cap',
                ],
            ],
        ],
        [
            'text'    => 'Acompanhamento Atividades',
            'icon'    => 'fas fa-fw fa-chart-line',
            'route'  => 'acompanhamento.index',
        ],
    ],


    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Here we can modify the menu filters of the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#610-menu-filters
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SearchFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\LangFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Here we can modify the plugins used inside the admin panel.
    |
    | For more detailed instructions you can look here:
    | https://github.com/jeroennoten/Laravel-AdminLTE/#611-plugins
    |
    */

    'plugins' => [
        [
            'name' => 'Datatables',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => '/vendor/datatables/js/jquery.datatables.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => true,
                    'location' => '/vendor/datatables/css/datatables.bootstrap4.min.css',
                ],
            ],
        ],
        [
            'name' => 'Select2',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => '/vendor/select2/js/select2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => true,
                    'location' => '/vendor/select2/css/select2.css',
                ],
            ],
        ],
        [
            'name' => 'Chartjs',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => '/vendor/chart.js/Chart.bundle.min.js',
                ],
            ],
        ],
        [
            'name' => 'Sweetalert2',
            'active' => true,
            'files' => [
                [
                    'type' => 'js',
                    'asset' => true,
                    'location' => '/vendor/sweetalert2/sweetalert2.min.js',
                ],
                [
                    'type' => 'css',
                    'asset' => true,
                    'location' => '/vendor/sweetalert2/sweetalert2.min.css',
                ],
            ],
        ],
        [
            'name' => 'Pace',
            'active' => false,
            'files' => [
                [
                    'type' => 'css',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-center-radar.min.css',
                ],
                [
                    'type' => 'js',
                    'asset' => false,
                    'location' => '//cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js',
                ],
            ],
        ],
    ],
];
