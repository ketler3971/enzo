<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class Evento extends Model
{
    protected $table = 'evento';
    public $primaryKey = 'id_evento';
    public $timestamps = true;
    protected $fillable = [
        'id_evento', 'dt_referencia', 'atividade_id', 'user_id', 'carga_horaria', 'pontuacao'
    ];

    public static function queryListAll($id)
    {
        return DB::table('evento')
            ->select(
                'atividade.nm_atividade',
                'evento.carga_horaria',
                'evento.pontuacao',
                'evento.id_evento',
                'evento.dt_referencia',
                'evento.atividade_id'
            )
            ->leftJoin('atividade', 'evento.atividade_id', '=', 'atividade.id_atividade')
            ->where('evento.user_id', '=', $id);
    }
    public static function listAll($id)
    {
        return DataTables::of(self::queryListAll($id))->make(true);
    }
    public static function excluir($id)
    {
        DB::table('evento')
            ->where('id_evento', '=', $id)
            ->delete();
    }
}
