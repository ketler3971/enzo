<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use \Yajra\DataTables\Facades\DataTables;

class Usuario extends Model
{
    protected $table = 'users';
    public $primaryKey = 'id_user';
    public $timestamps = true;
    protected $fillable = [
        'id_user', 'name', 'email', 'password', 'curso', 'status',
    ];

}
