<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Symfony\Component\CssSelector\Node\FunctionNode;
use Yajra\DataTables\Facades\DataTables;

class Atividade extends Model
{
    protected $table = 'atividade';
    public $primaryKey = 'id_atividade';
    public $timestamps = true;
    protected $fillable = [
        'id_atividade', 'nm_atividade', 'pontuacao', 'ds_atividade',
        'carga_horaria', 'grupo_id', 'user_id',
    ];

    public static function queryListAll($id)
    {
        return DB::table('atividade')
            ->select(
                'id_atividade',
                'nm_atividade',
                'nm_grupo',
                'atividade.pontuacao',
                'atividade.carga_horaria',
                'ds_atividade',
                'atividade.grupo_id'
            )
            ->leftJoin('grupo', 'grupo.id_grupo', '=', 'atividade.grupo_id')
            ->where('atividade.user_id', '=', $id);
    }
    public static function listAll($id)
    {
        return DataTables::of(self::queryListAll($id))->make(true);
    }
    public static function excluir($id)
    {

        return DB::table('atividade')
            ->where('id_atividade', '=', $id)
            ->delete();
    }
    public static function pontuacaoPorGrupo($id)
    {
        $query = DB::table('evento')
            ->select(
                'evento.grupo_id',
                'nm_grupo',
                DB::raw('sum(evento.pontuacao) as evento_pontuacao'),
                'grupo.pontuacao'
            )
            ->leftJoin('atividade', 'evento.atividade_id', '=', 'atividade.id_atividade')
            ->leftJoin('grupo', 'evento.grupo_id', '=', 'grupo.id_grupo')
            ->where('evento.user_id', '=', $id)
            ->groupBy('evento.grupo_id', 'nm_grupo', 'grupo.pontuacao');

        return Response::json($query->get());
    }
    public static function pontuacaoPorAtividade($id)
    {
        $query = DB::table('evento')
            ->select(
                'evento.atividade_id',
                'nm_atividade',
                DB::raw('sum(evento.pontuacao) as evento_pontuacao')
            )
            ->leftJoin('atividade', 'evento.atividade_id', '=', 'atividade.id_atividade')
            ->where('evento.user_id', '=', $id)
            ->groupBy('evento.atividade_id', 'nm_atividade');

        return Response::json($query->get());
    }
    public static function progressoGeral($id)
    {
        $query = DB::table('evento')
            ->select(
                DB::raw('(sum(evento.pontuacao)/sum(grupo.pontuacao))*100 as progresso')
            )
            ->leftJoin('grupo', 'evento.grupo_id', '=', 'grupo.id_grupo')
            ->where('evento.user_id', '=', $id);

        return $query->get()[0]->progresso;
    }
    public static function progressoPorGrupo($id)
    {
        $query = DB::table('evento')
            ->select(
                'grupo.nm_grupo',
                DB::raw('(sum(evento.pontuacao)/sum(grupo.pontuacao))*100 as progresso')
            )
            ->leftJoin('grupo', 'evento.grupo_id', '=', 'grupo.id_grupo')
            ->where('evento.user_id', '=', $id)
            ->groupBy('grupo.id_grupo', 'grupo.nm_grupo');

        return $query->get();
    }
}
