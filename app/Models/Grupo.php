<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use \Yajra\DataTables\Facades\DataTables;

class Grupo extends Model
{
    protected $table = 'grupo';
    public $primaryKey = 'id_grupo';
    public $timestamps = true;
    protected $fillable = [
        'id_grupo', 'nm_grupo', 'pontuacao', 'ds_grupo', 'user_id',
    ];

    public static function queryListAll($id_user)
    {
        return DB::table('grupo')
            ->where('user_id', '=', $id_user);
    }
    public static function listAll($id_user)
    {
        return DataTables::of(self::queryListAll($id_user))->make(true);
    }
    public static function excluir($id)
    {
        return DB::table('grupo')
            ->where('id_grupo', '=', $id)
            ->delete();
    }
}
