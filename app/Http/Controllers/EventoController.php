<?php

namespace App\Http\Controllers;

use App\Models\Atividade;
use App\Models\Evento;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class EventoController extends Controller
{
    private $view = 'evento.';

    public function index()
    {
        $user = Auth::user();
        $atividades = Atividade::queryListAll($user->id_user)->get();

        return view($this->view . 'index', ['atividades' => $atividades]);
    }
    public function listar()
    {
        $user = Auth::user();
        $eventos = Evento::listAll($user->id_user);
        return $eventos;
    }

    public function store(Request $request)
    {
        // $user = Auth::user();
        $certificado = $request->file('certificado')[0];
        $atividade = Atividade::find($request->atividade);
        $evento = new Evento();
        $evento->dt_referencia =  $request->data;
        $evento->atividade_id =  $request->atividade;
        $evento->carga_horaria =  $request->carga_horaria;
        $evento->user_id = Auth::user()->id_user;
        $evento->grupo_id = $atividade->grupo_id;
        $evento->nm_certificado = $certificado->getClientOriginalName();
        if ($atividade->carga_horaria != 0) {
            $evento->pontuacao = ($request->carga_horaria / $atividade->carga_horaria) * $atividade->pontuacao;
        } else {
            $evento->pontuacao = $atividade->pontuacao;
        }

        //concatenar com id do usuário
        $certificado->move(env('FILE_STORAGE') . '/1/' . date('Y-m-d-His'), $certificado->getClientOriginalName());
        $evento->save();

        //retorna para a tela atual
        return redirect()->back();
    }

    public function update(Request $request)
    {
        $certificado = $request->file('certificado')[0];
        $atividade = Atividade::find($request->atividade);
        $evento = Evento::find($request->id);
        $evento->dt_referencia =  $request->data;
        $evento->atividade_id =  $request->atividade;
        $evento->carga_horaria =  $request->carga_horaria;
        $evento->user_id = Auth::user()->id_user;
        $evento->grupo_id = $atividade->grupo_id;
        if ($atividade->carga_horaria != 0) {
            $evento->pontuacao = ($request->carga_horaria / $atividade->carga_horaria) * $atividade->pontuacao;
        } else {
            $evento->pontuacao = $atividade->pontuacao;
        }
        $datahora = str_replace(' ', '-', str_replace(':', '', $evento->created_at));
        if ($certificado != null) {
            //remove arquivo antigo
            unlink(env('FILE_STORAGE') . "/1/" . $datahora . '/' . $evento->nm_certificado);
            //concatenar com id do usuário
            //adiciona novo arquivo
            $certificado->move(env('FILE_STORAGE') . '/1/' . $datahora . '/', $certificado->getClientOriginalName());
            $evento->nm_certificado = $certificado->getClientOriginalName();
        }
        $evento->update();

        //retorna para a tela atual
        return redirect()->back();
    }
    public function excluir(Request $request)
    {
        try {
            Evento::excluir($request->id);
            return response()->json(
                [
                    'status' => 'success',
                    'mensagem' => 'Evento deletado com sucesso!'
                ]
            );
        } catch (Exception $e) {
            return response()->json(
                [
                    'status' => 'error',
                    'mensagem' => 'Oops, não foi possível deletar o evento',
                    500
                ]
            );
        }
    }

    public function viewCertificado($id)
    {
        $evento = Evento::find($id);
        $datahora = str_replace(' ', '-', str_replace(':', '', $evento->created_at));
        //trocar "/1" pelo id do usuario logado
        $file = env('FILE_STORAGE') . "/1/" . $datahora . '/' . $evento->nm_certificado;
        $headers = array('Content-Type: application/pdf',);

        return Response::download($file, 'filename.pdf', $headers, 'inline');
    }
}
