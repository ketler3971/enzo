<?php

namespace App\Http\Controllers;

use App\Models\Usuario;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $view = 'visao-admin.usuario.';


    public function create()
    {
        return view($this->view . 'create');
    }

    public function adicionar(Request $request)
    {
        // $user = Auth::user();
        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'curso' => $request->curso
        ]);
        $user->save();

        return redirect('/login');
    }
}
