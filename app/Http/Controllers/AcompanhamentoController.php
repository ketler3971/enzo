<?php

namespace App\Http\Controllers;

use App\Models\Atividade;
use App\Models\Evento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AcompanhamentoController extends Controller
{
    private $view = 'evento.';

    public function index()
    {
        $user = Auth::user();
        $progresso = (float) Atividade::progressoGeral($user->id_user);
        $grupos = Atividade::progressoPorGrupo($user->id_user);
        return view(
            $this->view . 'acompanhamento',
            [
                'progresso' => $progresso,
                'grupos' => $grupos,
            ]
        );
    }
    public function pontuacaoPorGrupo()
    {
        $user = Auth::user();
        $dados = Atividade::pontuacaoPorGrupo($user->id_user);
        return $dados;
    }
    public function pontuacaoPorAtividade()
    {
        $user = Auth::user();
        $dados = Atividade::pontuacaoPorAtividade($user->id_user);
        return $dados;
    }
}
