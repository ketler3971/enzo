<?php

namespace App\Http\Controllers;

use App\Models\Atividade;
use App\Models\Grupo;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AtividadeController extends Controller
{
    private $view = 'atividade.';

    public function index()
    {
        $user = Auth::user();
        $grupos = Grupo::queryListAll($user->id_user)->get();
        return view(
            $this->view . 'index',
            [
                'grupos' => $grupos
            ]
        );
    }
    public function listar()
    {
        $user = Auth::user();
        $atividades = Atividade::listAll($user->id_user);

        return $atividades;
    }
    public function store(Request $request)
    {
        $atividade = new Atividade([
            'nm_atividade' => $request->nome,
            'pontuacao' => $request->pontuacao,
            'ds_atividade' => $request->descricao,
            'carga_horaria' => $request->carga_horaria,
            'grupo_id' => $request->grupo,
            'user_id' =>  Auth::user()->id_user,
        ]);
        $atividade->save();

        return redirect()->back();
    }
    public function update(Request $request)
    {
        $atividade = Atividade::find($request->id);
        $atividade->nm_atividade = $request->nome;
        $atividade->pontuacao = $request->pontuacao;
        $atividade->carga_horaria = $request->carga_horaria;
        $atividade->grupo_id = $request->grupo;
        $atividade->user_id =  Auth::user()->id_user;
        $atividade->update();

        return redirect()->back();
    }
    public function excluir(Request $request)
    {
        try {
            Atividade::excluir($request->id);
            return response()->json(
                [
                    'status' => 'success',
                    'mensagem' => 'Atividade deletada com sucesso!'
                ]
            );
        } catch (Exception $e) {
            Log::debug($e->getMessage());
            return response()->json(
                [
                    'status' => 'error',
                    'mensagem' => 'Oops, não foi possível deletar a atividade!'
                ]
            );
        }
    }
}
