<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // // $user = ['tipo_usuario'=>'C'];
        // $user->tipo_usuario = 'C';

        if (false) {
            return view('home', ['isCoordenador' => true]);
        }
        if (true) {
            return view('home', ['isCoordenador' => true]);
        }
    }

}
