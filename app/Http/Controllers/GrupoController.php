<?php

namespace App\Http\Controllers;

use App\Models\Grupo;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class GrupoController extends Controller
{
    private $view = 'grupo.';

    public function index()
    {
        return view($this->view . 'index');
    }
    public function listar()
    {
        $user = Auth::user();
        $grupos = Grupo::listAll($user->id_user);
        return $grupos;
    }

    public function store(Request $request)
    {
        // $user = Auth::user();
        $grupo = new Grupo([
            'nm_grupo' => $request->nome,
            'pontuacao' => $request->pontuacao,
            'ds_grupo' => $request->descricao,
            // 'user_id' =>  $user->id_user,
            'user_id' =>  Auth::user()->id_user,
        ]);
        $grupo->save();

        //retorna para a tela atual
        return redirect()->back();
    }

    public function update(Request $request)
    {
        $grupo = Grupo::find($request->id);
        $grupo->nm_grupo = $request->nome;
        $grupo->pontuacao = $request->pontuacao;
        $grupo->ds_grupo = $request->descricao;
        $grupo->user_id = Auth::user()->id_user;
        $grupo->update();

        //retorna para a tela atual
        return redirect()->back();
    }
    public function excluir(Request $request)
    {
        try {
            Grupo::excluir($request->id);
            return response()->json(
                [
                    'status' => 'success',
                    'mensagem' => 'Grupo deletado com sucesso!'
                ]
            );
        } catch (Exception $e) {
            Log::debug($e->getMessage());
            return response()->json(
                [
                    'status' => 'error',
                    'mensagem' => 'Oops, não foi possível deletar o grupo'
                ]
            );
        }
    }
}
