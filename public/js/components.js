function dataRangePicker(select, single = false, ranges = false){
	let dataRange = false;
	if(ranges){
		dataRange = {ranges: {
			'Hoje': [moment(), moment()],
			'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
			'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
			'Este Mês': [moment().startOf('month'), moment().endOf('month')],
			'Ultimo Mês': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		}};
	}
	$(select).daterangepicker({
		autoUpdateInput: true,
		showDropdowns: true,
		applyButtonClasses :'btn-success',
		cancelButtonClasses : 'btn-danger',
		buttonClasses : 'btn btn-sm',
		linkedCalendars: false,
		singleDatePicker: single,
		ranges: dataRange.ranges,
		locale: {
			format: "DD/MM/YYYY",
			separator: " - ",
			applyLabel: "Aplicar",
			cancelLabel: "Cancelar",
			fromLabel: "De",
			toLabel: "Até",
			customRangeLabel: "Personalizado",
			daysOfWeek: [
				"Dom",
				"Seg",
				"Ter",
				"Qua",
				"Qui",
				"Sex",
				"Sáb"
			],
			monthNames: [
				"Janeiro",
				"Fevereiro",
				"Março",
				"Abril",
				"Maio",
				"Junho",
				"Julho",
				"Agosto",
				"Setembro",
				"Outubro",
				"Novembro",
				"Dezembro"
			],
			firstDay: 0,
		},
	});
	$(select).val('')
}

function montaMultiSelect(select){
    return $(select).multiSelect({
        selectableHeader: "<input type='text' class='search-input form-control form-control-sm "+
            "input-arrendondar' autocomplete='off' placeholder='Digite aqui o que deseja pesquisar'>",
        selectionHeader: "<input type='text' class='search-input form-control form-control-sm "+
            "input-arrendondar' autocomplete='off' placeholder='Digite aqui o que deseja pesquisar'>",
        afterInit: function(ms){
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
    
            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
            .on('keydown', function(e){
                if (e.which === 40){
                    that.$selectableUl.focus();
                    return false;
                }
            });
    
            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
            .on('keydown', function(e){
                if (e.which == 40){
                    that.$selectionUl.focus();
                    return false;
                }
            });
        },
        afterSelect: function(){
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function(){
            this.qs1.cache();
            this.qs2.cache();
        }
    });
}