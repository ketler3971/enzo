function filtrarResumo(url, filtro, inicio, fim, tabelas) {

	if( (inicio < '2017-08-01' && fim < '2017-08-01') || (inicio == "" && fim < '2017-08-01') )
	{
		filtro = "J";
	}
	
	if(
		(inicio < '2017-08-01' && fim >= '2017-08-01') ||
		(inicio < '2017-08-01' && fim == "" ) ||
		(inicio == "" && fim == "")
	){
		filtro = "T";
	}

	if( (inicio >= '2017-08-01' && fim >= '2017-08-01') || (inicio >= '2017-08-01' && fim == "") )
	{
		filtro = "A";
	}

	$('#filtroTipoSubvencao').val(filtro);
	
	$.ajax({
		url: url,
		type: 'GET',
		data: {
			"filtro" : filtro,
			"dt_inicio" : inicio,
			"dt_fim" : fim,
		}
	}).done(function() {
		for(i=0;i<tabelas.length;i++){
			tabelas[i].ajax.reload();
		}
		load_chart_resumo();
		load_chart_subvencao();
	})
} 

function filtrar(url, inicio, fim, tabelas) {	
	$.ajax({
		url: url,
		type: 'GET',
		data: {
			"dt_inicio" : inicio,
			"dt_fim" : fim,
		}
	}).done(function() {
		for(i=0;i<tabelas.length;i++){
			tabelas[i].ajax.reload();
		}
	})
} 
function filtrarMovimentacoes(url, inicio, fim, tabelas){
	$.ajax({
		url : url,
		type : 'GET',
		data: {
			"data_inicio" : inicio,
			"data_fim" : fim,
		}
	}).done(function(){
		for(i=0;i<tabelas.length;i++){
			tabelas[i].ajax.reload();
		}
		load_chart_resumo_movimentacao();
	})
}