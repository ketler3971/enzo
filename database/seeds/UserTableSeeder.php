<?php

use App\Models\Permission;
use App\Models\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $admin_role = Role::where('slug', 'admin')->first();
        // $admin_perm = Permission::where('slug', 'admin')->first();
        // $consult_role = Role::where('slug', 'consultor')->first();
        // $consult_perm = Permission::where('slug', 'preponderante-consultar')->first();
        // $create_perm = Permission::where('slug', 'preponderante-criar')->first();

        //Criação Admin Teste
        $admin = new User();
        $admin->name = 'Ketler Vinicius da Silva';
        $admin->email = 'ketler3971@gmail.com';
        $admin->password = bcrypt('99483576k');
        $admin->save();
    }
}
