<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAtividadeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atividade', function (Blueprint $table) {
            $table->increments('id_atividade');
            $table->string('nm_atividade', 255);
            $table->string('ds_atividade', 255);
            $table->float('carga_horaria', 5, 2);
            $table->integer('pontuacao');
            $table->timestamps();
            $table->integer('grupo_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('grupo_id')->references('id_grupo')->on('grupo');
            $table->foreign('user_id')->references('id_user')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atividade');
    }
}
