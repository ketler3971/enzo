<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evento', function (Blueprint $table) {
            $table->increments('id_evento');
            $table->date('dt_referencia');
            $table->timestamps();
            $table->integer('atividade_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->float('carga_horaria', 5, 2);
            $table->integer('pontuacao');

            $table->foreign('atividade_id')->references('id_atividade')->on('atividade');
            $table->foreign('user_id')->references('id_user')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evento');
    }
}
