<div class="form-row">
    <div class="form-group col-md-6">
        <label for="nome">Nome Grupo</label>
        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome Grupo" required>
    </div>
    <div class="form-group col-md-2">
        <label for="pontuacao">Pontuação</label>
        <input type="text" class="form-control" name="pontuacao" id="pontuacao" placeholder="Pontuação" required>
    </div>
    {{-- <div class="form-group col-md-4">
        <label for="pontuacao">Curso</label>
        <select id="inputEstado" name="curso" class="form-control">
            <option selected>Escolher...</option>
            <option id="cursoid">Curso 1</option>
        </select>
    </div> --}}
    <div class="form-group col-md-12">
        <label for="descricao">Descricao</label>
        <input type="text" class="form-control" name="descricao" id="descricao" placeholder="Descrição" required>
    </div>
</div>
