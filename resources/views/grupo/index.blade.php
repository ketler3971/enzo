@extends('adminlte::page')
@section('title', 'Grupos')
@section('adminlte_css')
@section('title')
<h1>Grupos</h1>
@endsection
@section('content')
<h3>Grupos</h3>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#addGrupo">
                        Novo Grupo
                    </button>
                </div>
                <div class="card-body">
                    <div class="table-responsive-sm">
                        <table class="table table-sm  table-bordered" id="listaGrupos">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 5%">#</th>
                                    <th scope="col" style="width: 30%">Nome Grupo</th>
                                    <th scope="col" style="width: 40%">Descricão</th>
                                    <th scope="col" style="width: 10%">Pontuação</th>
                                    <th scope="col" style="width: 7%">Editar</th>
                                    <th scope="col" style="width: 7%">Excluir</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- <td scope="row">1</td>
                                <td>Grupo 1</td>
                                <td>descrição bonita</td>
                                <td>30 pt.</td>
                                <td>
                                    <button type="button" class="btn btn-sm btn-outline-primary" data-toggle="modal"
                                        data-target="#editGrupo" data-nome="Grupo 1" data-descricao="descricao bonita"
                                        data-pontuacao="30">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                </td>
                                <td>
                                    <button class="btn btn-sm btn-outline-danger">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </td> --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('grupo.modal')
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        window.tabela = $('#listaGrupos').DataTable({
            language: {
                url: "{{asset('js/Portuguese-Brasil.json')}}"
            },
            ajax: "{{route('grupo.listar')}}",
            columns: [
                {data : 'id_grupo'},
                {data : 'nm_grupo'},
                {data : 'ds_grupo'},
                {data : 'pontuacao'},
                {render : function(data, type, row){
                    return "<button type=\"button\" class=\"btn btn-sm btn-outline-primary\" data-toggle=\"modal\""
                                +"data-target=\"#editGrupo\" data-id=\""+row.id_grupo+"\"data-nome=\""+row.nm_grupo+"\" "
                                +"data-id=\""+row.id_grupo+"\""
                                +"data-descricao=\""+row.ds_grupo+"\""
                                +"data-pontuacao=\""+row.pontuacao+"\">"
                                +"<i class=\"fas fa-edit\"></i>"
                            +"</button>"
                }},
                {render : function(data, type, row){
                    return "<button class=\"btn btn-sm btn-outline-danger\""
                            +"onclick=\"excluir("+row.id_grupo+");\">"
                                +"<i class=\"fas fa-trash-alt\"></i>"
                            +"</button>"
                }},
            ],
            order:0,
            fixedHeader: false,
            colReorder: false,
            responsive: true,
            columnDefs: [
                {
                    targets: '_all',
                    className: "text-center"
                },
                {
                    targets: [4,5],
                    orderable: false
                }
            ],
            drawCallback: function (settings) {
                $('[data-toggle="tooltip"]').tooltip();
            }
        });
        $('#editGrupo').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)

            var id = button.data('id')
            var nome = button.data('nome')
            var descricao = button.data('descricao')
            var pontuacao = button.data('pontuacao')
            var modal = $(this)

            modal.find('.modal-body #id').val(id);
            modal.find('.modal-body #nome').val(nome);
            modal.find('.modal-body #senha_certificado').val('');
            modal.find('.modal-body #descricao').val(descricao);
            modal.find('.modal-body #pontuacao').val(pontuacao);
        });
    });
    function excluir(id){
        Swal.fire({
            title: 'Você tem certeza que deseja excluir este grupo?',
            text: "Essa ação não pode ser revertida!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, tenho certeza!'
        }).then((result) => {
            $.ajax({
                type: "GET",
                url: "{{route('grupo.excluir')}}",
                data: { id : id},
                cache: false,
                success: function(response) {
                    if(response.status === 'success')
                    {
                        Swal.fire(
                            'Deletado!',
                            response.mensagem,
                            'success',
                            1500,
                        )
                        window.tabela.ajax.reload();
                    }
                    if(response.status === 'error'){
                        Swal.fire(
                            'Entre em contato com o setor T.I!',
                            response.mensagem,
                            'error'
                        )
                    }
                },
            });
        });
    }
</script>
@endsection
