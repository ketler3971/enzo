<div class="form-row">
    <div class="form-group col-md-6">
        <label for="nome">Nome Atividade</label>
        <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome Atividade" required>
    </div>
    <div class="form-group col-md-3">
        <label for="pontuacao">Pontuação</label>
        <input type="number" class="form-control" name="pontuacao" id="pontuacao" placeholder="Pontuação" required>
    </div>
    <div class="form-group col-md-3">
        <label for="carga-horaria">Carga horária</label>
        <input type="number" step="0.1" class="form-control" name="carga_horaria" id="cargaHoraria"
            placeholder="Carga horária" required>
    </div>
    <div class="form-group col-md-3">
        <label for="grupo">Grupo</label>
        <select name="grupo" id="grupo" class="form-control" name="grupo" required>
            @foreach ($grupos as $grupo)
            <option value="{{$grupo->id_grupo}}">{{$grupo->nm_grupo}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-9">
        <label for="descricao">Descricao</label>
        <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Descrição" required>
    </div>
</div>
