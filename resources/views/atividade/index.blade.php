@extends('adminlte::page')
@section('title', 'Atividades')
@section('adminlte_css')
@section('title')
<h1>Atividades</h1>
@endsection
@section('content')
<h3>Atividades</h3>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <button type="button" class="btn btn-sm btn-default" data-toggle="modal"
                        data-target="#addAtividade">
                        Nova Atividade
                    </button>
                </div>
                <div class="card-body">
                    <div class="table-responsive-sm">
                        <table class="table table-sm  table-bordered" id="listaAtividades">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 5%">#</th>
                                    <th scope="col" style="width: 20%">Nome Atividade</th>
                                    <th scope="col" style="width: 20%">Grupo</th>
                                    <th scope="col" style="width: 10%">Pontuação</th>
                                    <th scope="col" style="width: 10%">Carga Horária</th>
                                    <th scope="col" style="width: 25%">Descricão</th>
                                    <th scope="col" style="width: 5%">Editar</th>
                                    <th scope="col" style="width: 5%">Excluir</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- <td scope="row">1</td>
                                <td>Atividade 1</td>
                                <td>5 pt.</td>
                                <td>2h</td>
                                <td>descrição bonita</td>
                                <td>
                                    <button type="button" class="btn btn-sm btn-outline-primary" data-toggle="modal"
                                        data-target="#editAtividade" data-nome="Atividade 1"
                                        data-descricao="descricao bonita" data-pontuacao="5" data-cargaHoraria="2">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                </td>
                                <td>
                                    <button class="btn btn-sm btn-outline-danger">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </td> --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('atividade.modal')
@endsection
@section('js')
<script>
    $(document).ready(function () {
        window.tabela = $('#listaAtividades').DataTable({
            language: {
                url: "{{asset('js/Portuguese-Brasil.json')}}"
            },
            ajax:"{{route('atividade.listar')}}",
            columns:[
                {data : 'id_atividade'},
                {data : 'nm_atividade'},
                {data : 'nm_grupo'},
                {data : 'pontuacao'},
                {data : 'carga_horaria'},
                {data : 'ds_atividade'},
                {render : function(data, type, row){
                    return "<button type=\"butto\" class=\"btn btn-sm btn-outline-primary\""
                        +"data-toggle=\"modal\" data-id=\""+row.id_atividade+"\""
                        +"data-target=\"#editAtividade\" data-nome=\""+row.nm_atividade+"\""
                        +"data-descricao=\""+row.ds_atividade+"\" data-pontuacao=\""+row.pontuacao+"\""
                        +"data-carga=\""+row.carga_horaria+"\" data-grupo=\""+row.grupo_id+"\">"
                            +"<i class=\"fas fa-edit\"></i>"
                        +"</button>"
                }},
                {render : function(data, type, row){
                    return "<button class=\"btn btn-sm btn-outline-danger\""
                            +"onclick=\"excluir("+row.id_atividade+");\">"
                                +"<i class=\"fas fa-trash-alt\"></i>"
                            +"</button>"
                }},
            ],
            order:0,
            fixedHeader: false,
            colReorder: false,
            responsive: true,
            columnDefs: [
                {
                    targets: '_all',
                    className: "text-center"
                },
                {
                    targets: [4,5],
                    orderable: false
                }
            ],
            drawCallback: function (settings) {
                $('[data-toggle="tooltip"]').tooltip();
            }
        });
        $('#editAtividade').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)

            var id = button.data('id')
            var nome = button.data('nome')
            var descricao = button.data('descricao')
            var cargaHoraria = button.data('carga')
            var grupo = button.data('grupo')
            var pontuacao = button.data('pontuacao')
            var modal = $(this)

            modal.find('.modal-body #id').val(id);
            modal.find('.modal-body #nome').val(nome);
            modal.find('.modal-body #descricao').val(descricao);
            modal.find('.modal-body #grupo').val(grupo);
            modal.find('.modal-body #cargaHoraria').val(cargaHoraria);
            modal.find('.modal-body #pontuacao').val(pontuacao);
        })
    });
    function excluir(id){
        Swal.fire({
            title: 'Você tem certeza que deseja excluir esta atividade?',
            text: "Essa ação não pode ser revertida!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, tenho certeza!'
        }).then((result) => {
            $.ajax({
                type: "GET",
                url: "{{route('atividade.excluir')}}",
                data: { id : id},
                cache: false,
                success: function(response) {
                    if(response.status === 'success'){
                        Swal.fire(
                            'Deletada!',
                            response.mensagem,
                            'success',
                            1500,
                        )
                    window.tabela.ajax.reload();
                    }
                    if(response.status === 'error'){
                        Swal.fire(
                            'Entre em contato com o setor T.I!',
                            response.mensagem,
                            'success'
                        )
                    }
                },
            });
        });
    }
</script>
@endsection
