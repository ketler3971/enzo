@extends('adminlte::page')
@section('title', 'Certificado')
@section('adminlte_css')
@section('title')
<h1>Certificados</h1>
@endsection
@section('content')
<h3>Certificados</h3>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#addEvento">
                        Novo Certificado
                    </button>
                </div>
                <div class="card-body">
                    <div class="table-responsive-sm">
                        <table class="table table-sm  table-bordered" id="listaCertificados">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 20%">Atividade</th>
                                    <th scope="col" style="width: 15%">Carga Horária</th>
                                    <th scope="col" style="width: 10%">Pontuação</th>
                                    <th scope="col" style="width: 5%">Visualizar</th>
                                    <th scope="col" style="width: 5%">Editar</th>
                                    <th scope="col" style="width: 5%">Excluir</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- <td>Grupo 2</td>
                                <td>Doação de Sangue</td>
                                <td>2</td>
                                <td>5 pt.</td>
                                <td>
                                    <button type="button" class="btn btn-sm btn-outline-primary" data-toggle="modal"
                                        data-target="#viewCertificado">
                                        <i class="fas fa-eye"></i>
                                    </button>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-sm btn-outline-primary" data-toggle="modal"
                                        data-target="#editCertificado" data-nome="Aluno 1" data-grupo="Grupo 2"
                                        data-atividade="Doação de Sangue" data-cargaHoraria="2" data-pontuacao="5 pt.">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                </td>
                                <td>
                                    <button class="btn btn-sm btn-outline-danger">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </td> --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('evento.modal')
@endsection
@section('js')
<script>
    $(document).ready(function () {
        window.tabela = $('#listaCertificados').DataTable({
            language: {
                url: "{{asset('js/Portuguese-Brasil.json')}}"
            },
            ajax: "{{route('evento.listar')}}",
            columns:[
                {data:'nm_atividade'},
                {data:'carga_horaria'},
                {data:'pontuacao'},
                {render:function(data, type, row){
                    return "<a href=\"{{route('certificado.view', '')}}/"+row.id_evento+"\" target=\"_blank\" class=\"btn btn-sm btn-outline-primary\" >"
                                +"<i class=\"fas fa-eye\"></i>"
                            +"</a>"
                }},
                {render:function(data, type, row){
                    return "<button type=\"button\" class=\"btn btn-sm btn-outline-primary\" data-toggle=\"modal\""
                                +"data-target=\"#editEvento\""
                                +"data-dt_referencia=\""+row.dt_referencia+"\""
                                +"data-id=\""+row.id_evento+"\""
                                +"data-atividade=\""+row.atividade_id+"\""
                                +"data-carga=\""+row.carga_horaria+"\" data-pontuacao=\""+row.pontuacao+"\">"
                                +"<i class=\"fas fa-edit\"></i>"
                            +"</button>"
                }},
                {render:function(data, type, row){
                    return "<button onclick=\"excluir("+row.id_evento+");\" class=\"btn btn-sm btn-outline-danger\">"
                            +"<i class=\"fas fa-trash-alt\"></i>"
                        +"</button>"
                }},
            ],
            order:0,
            fixedHeader: false,
            colReorder: false,
            responsive: true,
            columnDefs: [
                {
                    targets: '_all',
                    className: "text-center"
                },
                {
                    targets: [4,5],
                    orderable: false
                }
            ],
            drawCallback: function (settings) {
                $('[data-toggle="tooltip"]').tooltip();
            }
        });
        $('#editEvento').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)

            var id = button.data('id')
            var atividade = button.data('atividade')
            var dt_referencia = button.data('dt_referencia')
            var cargaHoraria = button.data('carga')
            var pontuacao = button.data('pontuacao')
            var modal = $(this)

            modal.find('.modal-body #id').val(id);
            modal.find('.modal-body #data').val(dt_referencia);
            modal.find('.modal-body #atividade').val(atividade);
            modal.find('.modal-body #cargaHoraria').val(cargaHoraria);
            modal.find('.modal-body #pontuacao').val(pontuacao);
        })
    });
    function excluir(id){
        Swal.fire({
            title: 'Você tem certeza que deseja excluir este evento?',
            text: "Essa ação não pode ser revertida!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, tenho certeza!'
        }).then((result) => {
            $.ajax({
                type: "GET",
                url: "{{route('evento.excluir')}}",
                data: { id : id},
                cache: false,
                success: function(response) {
                    if(response.status === 'success'){
                        Swal.fire(
                            'Deletado!',
                            response.mensagem,
                            'success',
                            1500,
                        )
                    window.tabela.ajax.reload();
                    }
                    if(response.status === 'error'){
                        Swal.fire(
                            'Entre em contato com o setor T.I!',
                            response.mensagem,
                            'success'
                        )
                    }
                },
            });
        });
    }
</script>
@endsection
