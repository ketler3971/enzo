<div class="form-row">
    <div class="form-group col-md-6 ">
        <label for="grupo">Atividade</label>
        <select id="atividade" class="form-control" name="atividade" required>
            @foreach ($atividades as $atividade)
            <option value="{{$atividade->id_atividade}}">{{$atividade->nm_atividade}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="cargaHoraria">Carga Horária</label>
        <input type="number" step="0.1" class="form-control" name="carga_horaria" id="cargaHoraria"
            placeholder="Carga Horária" min="0" required>
    </div>
    <div class="form-group col-md-3">
        <label for="date">Data do Evento</label>
        <input type="date" class="form-control" id="data" name="data" placeholder="Data" required>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="certificado">Certificado:</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                </div>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="certificado" accept=".pdf" max="1"
                        name="certificado[]" @if(!$edit)required @endif>
                    <label class="custom-file-label" for="certificado" id="label-certificado">Selecionar
                        arquivo</label>
                </div>
            </div>
        </div>
    </div>
</div>
