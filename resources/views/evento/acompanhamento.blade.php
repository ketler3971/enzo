@extends('adminlte::page')
@section('adminlte_css')
@section('title', 'Progresso')
@section('content')
<div class="container-fluid">
    <div class="col-md-12">
        <h3>Acompanhamento de Progresso nas Atividades</h3>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-danger">
                <div class="card-header">
                    Progresso Geral
                </div>
                <div class="card-body">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                            role="progressbar" aria-valuenow="{{$progresso}}" aria-valuemin="0"
                            style="width: {{$progresso}}%;" aria-valuemax="100">
                            {{ $progresso}} %
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">
                    Progresso por grupo
                </div>
                <div class="card-body">
                    @foreach ($grupos as $grupo)
                    {{$grupo->nm_grupo}}
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-info"
                            role="progressbar" aria-valuenow="{{round($grupo->progresso, 2)}}" aria-valuemin="0"
                            style="width: {{round($grupo->progresso, 2)}}%;" aria-valuemax="100">
                            {{round($grupo->progresso, 2)}} %
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card card-danger">
                <div class="card-header">
                    Pontuação por grupo
                </div>
                <div class="card-body">
                    <canvas id="pontuacaoPorGrupo" width="350px" height="250px"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-default">
                <div class="card-header">
                    Pontuaçao por atividade
                </div>
                <div class="card-body">
                    <canvas id="pontuacaoPorAtividade" width="350px" height="250px"></canvas>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('js')
    <script>
        $(document).ready(function () {
        });
        function drawPontuacaoPorGrupo() {
            $.ajax({
                url: "{{ route('acompanhamento.pontuacaoPorGrupo') }}",
                dataType: 'json',
            }).done(function (results) {
                let labels = [], data=[], colors=[];
                var dynamicColors = function() {
                    var r = Math.floor(Math.random() * 255);
                    var g = Math.floor(Math.random() * 255);
                    var b = Math.floor(Math.random() * 255);
                    return "rgb(" + r + "," + g + "," + b + ")";
                };
                for (let prop in results) {
                    labels.push(results[prop].nm_grupo);
                    data.push(results[prop].evento_pontuacao);
                    colors.push(dynamicColors());
                }
                let tempData = {
                    labels : labels,
                    datasets : [{
                        data : data,
                        backgroundColor: colors,
                    },
                ]
                };
                let ctx = document.getElementById("pontuacaoPorGrupo").getContext("2d");
                ctx.height = 100;

                window.chartEfdCte = new Chart(ctx, {
                    type: 'pie',
                    data: tempData,
                    options: {
                        legend: {
                            display: true,
                            position: 'bottom',
                        },
                        responsive: true,
                        maintainAspectRatio: false,
                    }
                });
            });
        }
        function drawPontuacaoPorAtividade() {
            $.ajax({
                url: "{{ route('acompanhamento.pontuacaoPorAtividade') }}",
                dataType: 'json',
            }).done(function (results) {
                let labels = [], data=[], colors=[];
                var dynamicColors = function() {
                    var r = Math.floor(Math.random() * 255);
                    var g = Math.floor(Math.random() * 255);
                    var b = Math.floor(Math.random() * 255);
                    return "rgb(" + r + "," + g + "," + b + ")";
                };
                for (let prop in results) {
                    labels.push(results[prop].nm_atividade);
                    data.push(results[prop].evento_pontuacao);
                    colors.push(dynamicColors());
                }
                let tempData = {
                    labels : labels,
                    datasets : [{
                        data : data,
                        backgroundColor: colors,
                    },
                ]
                };
                let ctx = document.getElementById("pontuacaoPorAtividade").getContext("2d");
                ctx.height = 100;

                window.chartEfdCte = new Chart(ctx, {
                    type: 'pie',
                    data: tempData,
                    options: {
                        legend: {
                            display: true,
                            position: 'bottom',
                        },
                        responsive: true,
                        maintainAspectRatio: false,

                    }
                });
            });
        }
        drawPontuacaoPorGrupo();
        drawPontuacaoPorAtividade();
    </script>
    @endsection
