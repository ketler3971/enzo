<div class="form-group">
    <label>{{ $label }}</label>
    <input type="file" class="form-control-file" id="{{ $id }}" name="{{ $id }}" aria-describedby="fileHelp" accept="{{ $accept }}" required="{{ $required }}">
    <small id="fileHelp" class="form-text text-muted">{{ $legenda }}</small>
</div>
