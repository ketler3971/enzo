<button id="{{ $id }}" name="{{ $id }}" type="{{ $type }}" class="btn btn-{{ $class }} btn-sm"><i class="{{ $icone }} fa-fw"></i> {{ $nome }}</button>
