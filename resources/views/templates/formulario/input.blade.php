<div class="form-group">
    <label>{{ $label }}</label>
    <input type="{{ $type }}" class="form-control form-control-sm" id="{{ $id }}" name="{{ $id }}" placeholder="{{ $placeholder }}" value="{{ $value }}" {{ $required }} >
</div>
