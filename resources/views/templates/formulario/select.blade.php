<div class="form-group">
    <label>{{ $label }}</label>
    <select id="{{ $id }}" name="{{ $id }}" class="form-control" data-placeholder="Selecione um {{ $placeholder }}" required="{{ $required }}"></select>
</div>
