<div class="form-group">
    <label>Período *</label>
    <div class="input-group">
        <input type="date" class="form-control" id="inputFiltroPeriodoInicial" name="inputFiltroPeriodoInicial" required="required">
        <span class="input-group-addon">até</span>
        <input type="date" class="form-control" id="inputFiltroPeriodoFinal" name="inputFiltroPeriodoFinal" required="required">
        <span class="input-group-btn">
            @include('templates.formulario.button', ['id' => 'botao_consultar', 'type' => 'submit', 'class' => 'primary', 'icone' => 'fa fa-search', 'nome' => ''])
        </span>
    </div>
</div>