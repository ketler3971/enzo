<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Subvenções') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    @yield('styles')
</head>

<body>
    <div id="app">
        @auth
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <div class="navbar-header">

                    <!-- Branding Image -->
                    <!--{{ config('app.name', 'Subvenções') }}-->
                    <a class="navbar-brand" href="{{ url('/home') }}">
                        Subvenções
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @can('subvencao-criar')
                        <li class="nav-item"><a class="nav-link" href="{{ route('subvencao.create') }}">Novo
                                Processo</a></li>
                        @endcan
                        <li class="nav-item"><a class="nav-link"
                                href="{{ route('subvencao.index') }}">Acompanhamento</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('regraFiscal.index') }}">Regras
                                Fiscais</a></li>
                        @can('admin')
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" class="dropdown-toggle" data-toggle="dropdown"
                                role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                Usuários <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="nav-link" href="{{ route('user.create') }}">Novo Usuário</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="nav-link" href="{{ route('user.index') }}">Lista de Usuários</a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" class="dropdown-toggle" data-toggle="dropdown"
                                role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                Permissões <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="nav-link" href="{{ route('permission.create') }}">Nova Permissão</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="nav-link" href="{{ route('permission.index') }}">Lista de Permissões</a>
                                </li>
                            </ul>
                        </li>
                        @endcan
                    </ul>
                    
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav my-2 my-lg-0">
                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
                        @else
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" class="dropdown-toggle" data-toggle="dropdown"
                                role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item" href="{{ route('alterarSenha') }}">
                                        Alterar Senha
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <a class="dropdown-item" href="{{route('sobre.index')}}">Sobre</a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        @endauth

        @yield('content')
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery.mask.js') }}"></script>
    @yield('javascripts')

</body>

</html>