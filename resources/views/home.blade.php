@extends('adminlte::page')
@section('title', 'Home')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            <center>
            <img src="{{asset('imgs/logo-enzo.png')}}" class="img-responsive" />
            </center>
            <br/>
        </div>
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <strong>Olá, {{"Usuário"}}, seja bem-vindo(a) ao Enzo!</strong>
                </div>
                <div class="card-body">
                    <p>Desenvolvido pelos alunos do curso de Tecnologia em Sistemas para a
                        Internet da Universidade Tecnológica Federal do Paraná (UTFPR) do
                        campus toledo, o sistema Enzo tem como objetivo auxiliar o controle
                        e acompanhamento da realização de atividades complementares.
                        Com ele é possível cadastrar os grupos e categorias de atividades,
                        realizar o lançamento dos certificados obtidos em eventos para que
                        sejam devidamente conferidos pelo professor responsável do curso e
                        também acompanhar o progresso do  aluno em relação ao cumprimento
                        da carga horária estipulada.
                    </p>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection

