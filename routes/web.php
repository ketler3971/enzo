<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Auth::routes();


Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/curso', 'CursoController@index')->name('curso.index');
    Route::get('/curso/novo', 'CursoController@create')->name('curso.create');

    Route::get('/evento', 'EventoController@index')->name('evento.index');
    Route::post('/evento/novo', 'EventoController@store')->name('evento.store');
    Route::get('/evento/listar', 'EventoController@listar')->name('evento.listar');
    Route::get('/evento/excluir', 'EventoController@excluir')->name('evento.excluir');
    Route::post('/evento/update', 'EventoController@update')->name('evento.update');
    Route::get('/evento/certificado/{id}', 'EventoController@viewCertificado')->name('certificado.view');

    Route::get('/grupo', 'GrupoController@index')->name('grupo.index');
    Route::post('/grupo/novo', 'GrupoController@store')->name('grupo.store');
    Route::get('/grupo/listar', 'GrupoController@listar')->name('grupo.listar');
    Route::get('/grupo/excluir', 'GrupoController@excluir')->name('grupo.excluir');
    Route::get('/grupo/update', 'GrupoController@update')->name('grupo.update');

    Route::get('/atividade', 'AtividadeController@index')->name('atividade.index');
    Route::post('/atividade/novo', 'AtividadeController@store')->name('atividade.store');
    Route::get('/atividade/listar', 'AtividadeController@listar')->name('atividade.listar');
    Route::get('/atividade/excluir', 'AtividadeController@excluir')->name('atividade.excluir');
    Route::post('/atividade/update', 'AtividadeController@update')->name('atividade.update');

    Route::get('/acompanhamento', 'AcompanhamentoController@index')->name('acompanhamento.index');
    Route::get('/acompanhamento/pontuacaoPorGrupo', 'AcompanhamentoController@pontuacaoPorGrupo')->name('acompanhamento.pontuacaoPorGrupo');
    Route::get('/acompanhamento/pontuacaoPorAtividade', 'AcompanhamentoController@pontuacaoPorAtividade')->name('acompanhamento.pontuacaoPorAtividade');

    Route::get('/certificado', 'CertificadoCoordenadorController@index')->name('certificado.index');
    Route::get('/certificado/novo', 'CertificadoCoordenadorController@create')->name('certificado.create');

    Route::get('/certificado/upload', 'CertificadoController@upload')->name('certificado.upload');
});
Route::get('/admin/createUser', 'UsuarioController@create')->name('usuario.create');
Route::post('/usuario/adicionar', 'UserController@adicionar')->name('usuario.adicionar');
